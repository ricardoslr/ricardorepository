package br.com.testavenueproject.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.testavenueproject.vo.VOProduct;

@Path("/ProductService")
public class ProductService {

	@POST
	@Path("/createProduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<VOProduct> createProduct(VOProduct product) {
		
		List<VOProduct> listProduct = new ArrayList<VOProduct>();
		if(product != null) {
			listProduct.add(product);
		}
		
		return listProduct;
	}
	
	@POST
	@Path("/validateProduct")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean validateProduct(final VOProduct product) {
		
		boolean isValid = false;
		
		if(product != null) {
			isValid = true;
		}
		
		return isValid;
	}
	
}
